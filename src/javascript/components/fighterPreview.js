import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });
  
  // todo: show fighter info (image, name, health, etc.)
  if (fighter) {
    const { _id, source, ...info} = fighter;
    
    const infoTable = createElement({
      tagName: 'div',
      className: 'fighter-preview___info-table'
    });
    const contents = Object.entries(info).reduce(function (str, pair) {
      const [key, value] = pair;
      return str + `<tr><td><span>${key}</span></td><td><span>${info[key]}</span></td></tr>`;
    }, '');
    infoTable.innerHTML = `<table>${contents}</table>`;
    
    fighterElement.appendChild(infoTable);
    
    const imgElement = createFighterImage(fighter);
    fighterElement.appendChild(imgElement);
  }
  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = { 
    src: source, 
    title: name,
    alt: name 
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}

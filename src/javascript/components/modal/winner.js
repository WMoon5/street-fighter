import { showModal } from './modal';
import { createFighterImage } from '../fighterPreview';
import { createFighters } from '../fightersView';
import { fighterService } from '../../services/fightersService';


export function showWinnerModal(fighter) {
  
  const reloadArena = async () => {
    
    const rootElement = document.getElementById('root');
    rootElement.innerHTML = '';
    
    try {
      const fighters = await fighterService.getFighters();
      const fightersElement = createFighters(fighters);

      rootElement.appendChild(fightersElement);
    } catch (error) {
      console.warn(error);
      rootElement.innerText = 'Failed to load data';
    }
  } 
  // call showModal function
  const bodyElement = createFighterImage(fighter);
  showModal({
    title: `The winner is ${fighter.name} !!!`,
    bodyElement,
    onClose: reloadArena
  });
}

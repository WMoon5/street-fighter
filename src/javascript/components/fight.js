import { controls } from '../../constants/controls';

export async function fight(firstFighter, secondFighter) {
  var leftHealth = firstFighter.health;
  var rightHealth = secondFighter.health;
    
  var states = {
    leftAttack: { is: false, allowed: true, side: 'left', text: 'SHOT', def: 1, strokes: [controls.PlayerOneAttack] },
    leftBlock: { is: false, allowed: true, side: 'left', text: 'BLOCK', def: 5, strokes: [controls.PlayerOneBlock] },
    leftBlow: { is: false, allowed: true, side: 'left', text: '<<<BLOW>>>', def: 10000, strokes: controls.PlayerOneCriticalHitCombination },
    rightAttack: { is: false, allowed: true, side: 'right', text: 'SHOT', def: 1, strokes: [controls.PlayerTwoAttack] },
    rightBlock: { is: false, allowed: true, side: 'right', text: 'BLOCK', def: 5, strokes: [controls.PlayerTwoBlock] },
    rightBlow: { is: false, allowed: true, side: 'right', text: '<<<BLOW>>>', def: 10000, strokes: controls.PlayerTwoCriticalHitCombination }
  }
  
  var keys_state = new Map();
  
  function setupFight() {
    let keyValues = Object.keys(controls).reduce(function (r, k) { return r.concat(controls[k]); }, []);
    keyValues.forEach( (val) => keys_state.set(val, false) );
    //
    window.addEventListener('keydown', (event) => {
      if (keys_state.has(event.code)) {
        keys_state.set(event.code, true);
      } 
    }, false);
  }
  
  setupFight();
    
  return new Promise((resolve) => {
    // resolve the promise with the winner when fight is over
    const interval = setInterval(() => {
      if (rightHealth == 0) {
        clearInterval(interval);
        resolve(firstFighter);
      }
      if (leftHealth == 0) {
        clearInterval(interval);
        resolve(secondFighter);
      }
      Object.keys(states).forEach((k) => {
        const state = states[k];
        if (!state.is) {
          const occured = state.strokes.every((key) => keys_state.get(key));
          if (occured) {
            if (!state.allowed) {
              console.log('not allowed');
              return;
            }
            console.log(k);
            //
            if (k == 'leftAttack' || k == 'leftBlow') {
              if (states['leftBlock'].is) {
                console.log(`can't attack from block`);
                return;
              }
            }
            if (k == 'rightAttack' || k == 'rightBlow') {
              if (states['rightBlock'].is) {
                console.log(`can't attack from block`);
                return;
              }
            }
            //
            state.is = true;
            state.allowed = false;
            setTimeout(() => state.allowed = true, state.def);
            // if (k == 'leftBlock' || k == 'rightBlock') {
            //   setTimeout(() => state.is = false, state.def);
            // }
            //
            const elem = document.getElementsByClassName(`arena___${state.side}-fighter-action`)[0];
            elem.innerText = state.text;
            setTimeout(() => elem.innerText = '', 2000);
            //
          }
        }
      });
      keys_state.forEach( (v, k) => keys_state.set(k, false) );
      //
      if (states['leftAttack'].is) {
        const damage = (states['rightBlock'].is) ? getDamage(firstFighter, secondFighter) : getHitPower(firstFighter);
        rightHealth -= damage;
        states['leftAttack'].is = false;
      }
      if (states['rightAttack'].is) {
        const damage = (states['leftBlock'].is) ? getDamage(secondFighter, firstFighter) : getHitPower(secondFighter);
        leftHealth -= damage;
        states['rightAttack'].is = false;
      }
      //
      if (states['leftBlow'].is) {
        const damage = 2 * firstFighter.attack;
        rightHealth -= damage;
        states['leftBlow'].is = false;
      }
      if (states['rightBlow'].is) {
        const damage = 2 * secondFighter.attack;
        leftHealth -= damage;
        states['rightBlow'].is = false;
      }
      //
      states['leftBlock'].is = false;
      states['rightBlock'].is = false;
      //
      rightHealth = (rightHealth < 0) ? 0 : rightHealth;
      const rightBar = document.getElementById('right-fighter-indicator');
      rightBar.style.width = `${rightHealth / secondFighter.health * 100}%`;
      
      leftHealth = (leftHealth < 0) ? 0 : leftHealth;
      const leftBar = document.getElementById('left-fighter-indicator');
      leftBar.style.width = `${leftHealth / firstFighter.health * 100}%`;
      //
    }, 20);
  });
}

export function getDamage(attacker, defender) {
  // return damage
  const damage = getHitPower(attacker) - getBlockPower(defender);
  return (damage > 0) ? damage : 0;
}

export function getHitPower(fighter) {
  // return hit power
  const criticalHitChance = 1 + Math.random();
  return fighter.attack * criticalHitChance;
}

export function getBlockPower(fighter) {
  // return block power
  const dodgeChance = 1 + Math.random();
  return fighter.defence * dodgeChance;
}


